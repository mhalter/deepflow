import numpy as np
import tensorflow as tf
import tensorflow_gnn as tfgnn
from tensorflow_gnn.models import vanilla_mpnn

import os
from typing import Tuple
from argparse import Namespace

from data import get_dataset_for_graph, shuffle
from utils import get_parameters, Logger

tf.get_logger().setLevel('ERROR')


def get_initial_map_features(hidden_size, activation='relu'):
    def node_sets_fn(node_set, node_set_name):
        if node_set_name == 'nodes':
            X = tf.stack([node_set['x'], node_set['y'], node_set['z'], node_set['t']], 1)
            return tf.keras.layers.Dense(units=hidden_size // 4, activation=activation)(tf.reshape(X, (-1, 4)))

    return tfgnn.keras.layers.MapFeatures(node_sets_fn=node_sets_fn,
                                          name='graph_embedding')


def get_final_map_features(activation='relu'):
    def node_sets_fn(node_set, node_set_name):
        if node_set_name == 'nodes':
            return tf.keras.layers.Dense(units=4, activation=activation)(node_set[tfgnn.HIDDEN_STATE])

    return tfgnn.keras.layers.MapFeatures(node_sets_fn=node_sets_fn,
                                          name='output_layer')


class MPNN(tf.keras.layers.Layer):
    def __init__(self, hidden_channels, depth, name='vanilla_mpnn', **kwargs):
        self.hidden_channels = hidden_channels
        self.depth = depth
        super().__init__(name=name, **kwargs)

        self.mp_layers = [self._mp_factory(name=f'message_passing_{i}') for i in range(depth)]

    def _mp_factory(self, name):
        return vanilla_mpnn.VanillaMPNNGraphUpdate(units=self.hidden_channels,
                                                   message_dim=self.hidden_channels,
                                                   receiver_tag=tfgnn.TARGET,
                                                   node_set_names=['nodes'],
                                                   reduce_type='mean')

    def get_config(self):
        config = super().get_config()
        config.update({
            'hidden_size': self.hidden_channels,
            'depth': self.depth
        })
        return config

    def call(self, graph_tensor):
        for layer in self.mp_layers:
            graph_tensor = layer(graph_tensor)
        return graph_tensor


def vanilla_mpnn_model(graph_tensor_spec, input_state_fn, pass_messages_fn, output_state_fn):
    graph_tensor = tf.keras.layers.Input(type_spec=graph_tensor_spec)
    graph_embedding = input_state_fn(graph_tensor)
    hidden_graph = pass_messages_fn(graph_embedding)
    output_graph = output_state_fn(hidden_graph)
    return tf.keras.Model(inputs=graph_tensor, outputs=output_graph)


class PINN(tf.keras.Model):
    def __init__(self, params: Namespace, lb: np.ndarray, ub: np.ndarray, data_idx: np.ndarray,
                 graph_spec: tfgnn.GraphTensorSpec, w_init: str = 'glorot_normal', b_init: str = 'zeros'):
        super().__init__()
        self.seed = params.seed
        self.in_dim = 4
        self.out_dim = 4
        self.hidden_channels = params.hidden_channels
        self.depth = params.depth
        self.activation = params.activation
        self.batch_norm = params.batch_norm
        self.w_init = w_init
        self.b_init = b_init
        self.learning_rate = params.learning_rate
        self.weight_data = params.w_data
        self.weight_residual = params.w_residual
        self.lb = tf.convert_to_tensor(lb)
        self.ub = tf.convert_to_tensor(ub)
        self.data_idx = tf.constant(data_idx, dtype=tf.int32)
        self.graph_spec = graph_spec

        self.net = self.net_init()
        self.logger = Logger(params)

        self.optimizer = tf.keras.optimizers.Adam(learning_rate=self.learning_rate)

    def net_init(self) -> tf.keras.layers.Layer:
        mpnn = MPNN(hidden_channels=self.hidden_channels, depth=self.depth)
        net = vanilla_mpnn_model(graph_tensor_spec=self.graph_spec,
                                 input_state_fn=get_initial_map_features(self.hidden_channels, self.activation),
                                 pass_messages_fn=mpnn,
                                 output_state_fn=get_final_map_features(self.activation))
        return net

    def gradients(self, batch: tfgnn.GraphTensor):
        with tf.GradientTape(persistent=True) as tp:
            x = batch.node_sets['nodes'].features['x']
            y = batch.node_sets['nodes'].features['y']
            z = batch.node_sets['nodes'].features['z']
            t = batch.node_sets['nodes'].features['t']

            tp.watch(x)
            tp.watch(y)
            tp.watch(z)
            tp.watch(t)

            graph = self.net(batch)
            F = graph.node_sets['nodes'].features[tfgnn.HIDDEN_STATE]

            u = F[:, 0]
            v = F[:, 1]
            w = F[:, 2]
            p = F[:, 3]

            u_x = tp.gradient(u, x)
            u_y = tp.gradient(u, y)
            u_z = tp.gradient(u, z)
            v_x = tp.gradient(v, x)
            v_y = tp.gradient(v, y)
            v_z = tp.gradient(v, z)

            w_x = tp.gradient(w, x)
            w_y = tp.gradient(w, y)
            w_z = tp.gradient(w, z)

        U = tf.stack([u, v, w], 1)
        U_x = tf.stack([u_x, v_x, w_x], 1)
        U_y = tf.stack([u_y, v_y, w_y], 1)
        U_z = tf.stack([u_z, v_z, w_z], 1)

        p_x = tp.gradient(p, x)
        p_y = tp.gradient(p, y)
        p_z = tp.gradient(p, z)

        u_t = tp.gradient(u, t)
        v_t = tp.gradient(v, t)
        w_t = tp.gradient(w, t)

        U_t = tf.stack([u_t, v_t, w_t], 1)

        u_xx = tp.gradient(u_x, x)
        v_xx = tp.gradient(v_x, x)
        w_xx = tp.gradient(w_x, x)

        U_xx = tf.stack([u_xx, v_xx, w_xx], 1)

        u_yy = tp.gradient(u_y, y)
        v_yy = tp.gradient(v_y, y)
        w_yy = tp.gradient(w_y, y)

        U_yy = tf.stack([u_yy, v_yy, w_yy], 1)

        u_zz = tp.gradient(u_z, z)
        v_zz = tp.gradient(v_z, z)
        w_zz = tp.gradient(w_z, z)

        U_zz = tf.stack([u_zz, v_zz, w_zz], 1)

        del tp
        return tf.reshape(U, (-1, 3)), tf.reshape(U_t, (-1, 3)), tf.reshape(U_x, (-1, 3)), tf.reshape(U_xx, (-1, 3)), \
            tf.reshape(U_y, (-1, 3)), tf.reshape(U_yy, (-1, 3)), tf.reshape(U_z, (-1, 3)), tf.reshape(U_zz, (-1, 3)), \
            tf.reshape(p, (-1,)), tf.reshape(p_x, (-1,)), tf.reshape(p_y, (-1,)), tf.reshape(p_z, (-1,))

    def pde(self, batch: tfgnn.GraphTensor) -> tf.Tensor:
        U, U_t, U_x, U_xx, U_y, U_yy, U_z, U_zz, _, p_x, p_y, p_z = self.gradients(batch)

        mu = 3.5e-3
        p0 = 994
        if U.shape[0] > 0:
            e1 = U_t[:, 0] + (U[:, 0] * U_x[:, 0] + U[:, 1] * U_y[:, 0] + U[:, 2] * U_z[:, 0]) + p_x \
                 - mu / p0 * (U_xx[:, 0] + U_yy[:, 0] + U_zz[:, 0])
            e2 = U_t[:, 1] + (U[:, 0] * U_x[:, 1] + U[:, 1] * U_y[:, 1] + U[:, 2] * U_z[:, 1]) + p_y \
                 - mu / p0 * (U_xx[:, 1] + U_yy[:, 1] + U_zz[:, 1])
            e3 = U_t[:, 2] + (U[:, 0] * U_x[:, 2] + U[:, 1] * U_y[:, 2] + U[:, 2] * U_z[:, 2]) + p_z \
                 - mu / p0 * (U_xx[:, 2] + U_yy[:, 2] + U_zz[:, 2])
            e4 = U_x[:, 0] + U_y[:, 1] + U_z[:, 2]
        else:
            e1 = tf.zeros(1, dtype='float32')
            e2 = tf.zeros(1, dtype='float32')
            e3 = tf.zeros(1, dtype='float32')
            e4 = tf.zeros(1, dtype='float32')
        return e1 + e2 + e3 + e4

    def loss_prediction(self, batch: tfgnn.GraphTensor) -> tf.Tensor:
        graph = self.net(batch)
        F = graph.node_sets['nodes'].features[tfgnn.HIDDEN_STATE]
        F_truth = batch.node_sets['nodes'].features['F']
        U = tf.gather(F_truth[:, :3], self.data_idx, axis=0)
        p = tf.gather(F_truth[:, 3], self.data_idx, axis=0)
        U_hat = tf.reshape(tf.gather(F[:, :3], self.data_idx, axis=0), (-1, 3))
        p_hat = tf.reshape(tf.gather(F[:, 3], self.data_idx, axis=0), (-1,))
        return tf.reduce_sum(tf.reduce_mean(tf.square(U - U_hat), axis=0)) + tf.reduce_mean(tf.square(p - p_hat))

    def loss_residual(self, batch: tfgnn.GraphTensor) -> tf.Tensor:
        return tf.reduce_mean(tf.square(self.pde(batch)))

    def loss_global(self, batch_d: tfgnn.GraphTensor,
                    batch_r: tfgnn.GraphTensor) -> Tuple[tf.Tensor, tf.Tensor, tf.Tensor]:
        loss_prediction = self.loss_prediction(batch_d)
        loss_residual = self.loss_residual(batch_r)
        loss = self.weight_data * loss_prediction + self.weight_residual * loss_residual
        return loss, loss_prediction, loss_residual

    def loss_gradient(self, batch_d: tfgnn.GraphTensor,
                      batch_r: tfgnn.GraphTensor) -> Tuple[tf.Tensor, tf.Tensor, tf.Tensor, tf.Tensor]:
        with tf.GradientTape(persistent=True) as tp:
            loss, loss_prediction, loss_residual = self.loss_global(batch_d, batch_r)

        grad = tp.gradient(loss, self.net.trainable_variables)
        del tp
        return grad, loss, loss_prediction, loss_residual

    def gradient_descent(self, batch_d: tfgnn.GraphTensor, batch_r: tfgnn.GraphTensor):
        grad, loss, loss_prediction, loss_residual = self.loss_gradient(batch_d, batch_r)
        self.optimizer.apply_gradients(zip(grad, self.net.trainable_variables))
        return loss, loss_prediction, loss_residual

    def train(self, dataset_data: tf.data.Dataset, dataset_residual: tf.data.Dataset, epoch=10 ** 2, tol=1e-5):
        self.logger.log_train_start(self.net)
        for ep in range(epoch):
            n_batches = 0
            ep_loss = 0.0
            ep_loss_data = 0.0
            ep_loss_residual = 0.0
            for batch_data, batch_residual in zip(dataset_data, dataset_residual):
                n_batches += 1
                loss_batch, loss_prediction, loss_residual = self.gradient_descent(batch_data, batch_residual)
                ep_loss += loss_batch
                ep_loss_data += loss_prediction
                ep_loss_residual += loss_residual

            self.logger.log_epoch(ep_loss / n_batches, ep_loss_data / n_batches, ep_loss_residual / n_batches)
            if ep_loss < tol:
                break

    def predict(self, dataset: tf.data.Dataset):
        mu = 3.5e-3
        U_p = tf.zeros((0, 3), dtype='float32')
        U_tr = tf.zeros((0, 3), dtype='float32')
        U_X_p = tf.zeros((0, 3, 4), dtype='float32')
        U_XX_p = tf.zeros((0, 3, 3), dtype='float32')
        p_p = tf.zeros((0), dtype='float32')
        p_t = tf.zeros((0), dtype='float32')
        p_X_p = tf.zeros((0, 3), dtype='float32')
        X_ = tf.zeros((0, 4), dtype='float32')
        wss_ = tf.zeros((0, 3), dtype='float32')
        wss_p = tf.zeros((0, 3), dtype='float32')
        for batch in dataset:
            U, U_t, U_x, U_xx, U_y, U_yy, U_z, U_zz, p, p_x, p_y, p_z = self.gradients(batch)
            U_X_ = tf.stack([U_x, U_y, U_z, U_t], axis=2)
            p_X_ = tf.stack([p_x, p_y, p_z], axis=1)
            U_XX_ = tf.stack([U_xx, U_yy, U_zz], axis=2)
            U_p = tf.concat([U_p, U], axis=0)
            U_tr = tf.concat([U_tr, batch.node_sets['nodes'].features['F'][:, 0:3]], axis=0)
            U_X_p = tf.concat([U_X_p, U_X_], axis=0)
            U_XX_p = tf.concat([U_XX_p, U_XX_], axis=0)
            p_p = tf.concat([p_p, p], axis=0)
            p_t = tf.concat([p_t, batch.node_sets['nodes'].features['F'][:, 3]], axis=0)
            p_X_p = tf.concat([p_X_p, p_X_], axis=0)
            x = batch.node_sets['nodes'].features['x']
            y = batch.node_sets['nodes'].features['y']
            z = batch.node_sets['nodes'].features['z']
            t = batch.node_sets['nodes'].features['t']
            X_ = tf.concat([X_, tf.reshape(tf.stack([x, y, z, t], 1), (-1, 4))], axis=0)
            normals_ = batch.node_sets['nodes'].features['normals']
            wss_ = tf.concat([wss_, batch.node_sets['nodes'].features['wss']], axis=0)
            u = U_X_[:, 0, 0:3]
            v = U_X_[:, 1, 0:3]
            w = U_X_[:, 2, 0:3]
            wss = tf.stack([tf.math.reduce_sum(u * normals_, axis=-1),
                            tf.math.reduce_sum(v * normals_, axis=-1),
                            tf.math.reduce_sum(w * normals_, axis=-1)], axis=-1)
            wss_p = tf.concat([wss_p, wss * mu], axis=0)
        error = tf.reduce_sum(tf.reduce_mean(tf.square(U_p - U_tr), axis=(0, 1))) + \
                tf.reduce_mean(tf.square(p_p - p_t))
        self.logger.log_train_end(error)
        uniques, _ = tf.unique(X_[:, 3])
        T = uniques.shape[0]
        X_ = tf.reshape(X_, (T, -1, 4))
        U_p = tf.reshape(U_p, (T, -1, 3))
        U_tr = tf.reshape(U_tr, (T, -1, 3))
        U_X_p = tf.reshape(U_X_p, (T, -1, 3, 4))
        U_XX_p = tf.reshape(U_XX_p, (T, -1, 3, 3))
        p_p = tf.reshape(p_p, (T, -1))
        p_t = tf.reshape(p_t, (T, -1))
        p_X_p = tf.reshape(p_X_p, (T, -1, 3))
        wss_p = tf.reshape(wss_p, (T, -1, 3))
        wss_ = tf.reshape(wss_, (T, -1, 3))
        return X_.numpy(), U_p.numpy(), U_tr.numpy(), U_X_p.numpy(), U_XX_p.numpy(), \
            p_p.numpy(), p_t.numpy(), p_X_p.numpy(), wss_p.numpy(), wss_.numpy()


if __name__ == '__main__':
    parameters = get_parameters()
    file = os.path.join(parameters.path, 'data/numpy', parameters.case_name, '{}.npz'.format(parameters.set_name))

    tf.random.set_seed(seed=parameters.seed)

    ds_data, ds_res, ds, lower_bound, upper_bound, d_idx, graph_spec = get_dataset_for_graph(file=file,
                                                                                             t_frac=parameters.training_fraction,
                                                                                             seed=parameters.seed)
    ds_data, ds_res = shuffle(data=ds_data, residuals=ds_res)
    pinn = PINN(params=parameters,
                lb=lower_bound,
                ub=upper_bound,
                data_idx=d_idx,
                graph_spec=graph_spec)

    pinn.train(dataset_data=ds_data, dataset_residual=ds_res, epoch=parameters.max_epochs)

    X, U_pred, U_truth, U_X, U_XX, p_pred, p_truth, p_X, wss_pred, wss_truth = pinn.predict(dataset=ds)

    file = os.path.join(parameters.path, 'results', parameters.id)

    np.savez(file=file, X=X, U_truth=U_truth, U_pred=U_pred, U_X=U_X, U_XX=U_XX,
             p_truth=p_truth, p_pred=p_pred, p_X=p_X, wss_pred=wss_pred, wss_truth=wss_truth)
