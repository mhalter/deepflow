import numpy as np
import tensorflow as tf

import os
from typing import Tuple
from argparse import Namespace

from data import get_dataset, shuffle_and_batch, get_eval_dataset
from utils import get_parameters, Logger

tf.get_logger().setLevel('ERROR')


class PINN(tf.keras.Model):
    def __init__(self, params: Namespace, lb: np.ndarray, ub: np.ndarray,
                 w_init: str = 'glorot_normal', b_init: str = 'zeros'):
        super().__init__()
        self.seed = params.seed
        self.in_dim = 4
        self.out_dim = 13
        self.hidden_channels = params.hidden_channels
        self.depth = params.depth
        self.activation = params.activation
        self.batch_norm = params.batch_norm
        self.w_init = w_init
        self.b_init = b_init
        self.learning_rate = params.learning_rate
        self.weight_data = params.w_data
        self.weight_residual = params.w_residual
        self.weight_boundary = params.w_boundary
        self.lb = tf.convert_to_tensor(lb)
        self.ub = tf.convert_to_tensor(ub)

        self.net = self.net_init()
        self.logger = Logger(params)

        self.optimizer = tf.keras.optimizers.Adam(learning_rate=self.learning_rate)
        self.use_p = True

    def net_init(self) -> tf.keras.Sequential:
        net = tf.keras.Sequential()
        net.add(tf.keras.layers.InputLayer(self.in_dim))
        net.add(tf.keras.layers.Lambda(lambda x: 2. * (x - self.lb) / (self.ub - self.lb) - 1.))
        if self.batch_norm:
            for _ in range(self.depth - 1):
                net.add(tf.keras.layers.Dense(units=self.hidden_channels,
                                              activation=self.activation,
                                              use_bias=False,
                                              kernel_initializer=self.w_init,
                                              bias_initializer=self.b_init,
                                              kernel_regularizer=None,
                                              bias_regularizer=None,
                                              activity_regularizer=None,
                                              kernel_constraint=None,
                                              bias_constraint=None))
                net.add(tf.keras.layers.BatchNormalization(axis=-1,
                                                           momentum=0.99,
                                                           epsilon=0.001,
                                                           center=True,
                                                           scale=True,
                                                           beta_initializer='zeros',
                                                           gamma_initializer='ones',
                                                           moving_mean_initializer='zeros',
                                                           moving_variance_initializer='ones',
                                                           beta_regularizer=None,
                                                           gamma_regularizer=None,
                                                           beta_constraint=None,
                                                           gamma_constraint=None))

        else:
            for _ in range(self.depth - 1):
                net.add(tf.keras.layers.Dense(units=self.hidden_channels,
                                              activation=self.activation,
                                              use_bias=True,
                                              kernel_initializer=self.w_init,
                                              bias_initializer=self.b_init,
                                              kernel_regularizer=None,
                                              bias_regularizer=None,
                                              activity_regularizer=None,
                                              kernel_constraint=None,
                                              bias_constraint=None))
        net.add(tf.keras.layers.Dense(units=self.out_dim))
        return net

    def gradients(self, x: tf.Tensor, y: tf.Tensor, z: tf.Tensor, t: tf.Tensor):
        with tf.GradientTape(persistent=True) as tp:
            tp.watch(x)
            tp.watch(y)
            tp.watch(z)
            tp.watch(t)

            F = self.net(tf.stack([x, y, z, t], 1))

            u = F[:, 0]
            v = F[:, 1]
            w = F[:, 2]
            p = F[:, 3]
            # rho_xx = F[:, 4]
            # rho_xy = F[:, 5]
            # rho_xz = F[:, 6]
            # rho_yx = F[:, 7]
            # rho_yy = F[:, 8]
            # rho_yz = F[:, 9]
            # rho_zx = F[:, 10]
            # rho_zy = F[:, 11]
            # rho_zz = F[:, 12]

            u_x = tp.gradient(u, x)
            u_y = tp.gradient(u, y)
            u_z = tp.gradient(u, z)

            v_x = tp.gradient(v, x)
            v_y = tp.gradient(v, y)
            v_z = tp.gradient(v, z)

            w_x = tp.gradient(w, x)
            w_y = tp.gradient(w, y)
            w_z = tp.gradient(w, z)

            # rho_xx_x = tp.gradient(rho_xx, x)
            # rho_xy_y = tp.gradient(rho_xy, y)
            # rho_xz_z = tp.gradient(rho_xz, z)
            # rho_yx_x = tp.gradient(rho_yx, x)
            # rho_yy_y = tp.gradient(rho_yy, y)
            # rho_yz_z = tp.gradient(rho_yz, z)
            # rho_zx_x = tp.gradient(rho_zx, x)
            # rho_zy_y = tp.gradient(rho_zy, y)
            # rho_zz_z = tp.gradient(rho_zz, z)

        U = tf.stack([u, v, w], 1)

        U_x = tf.stack([u_x, v_x, w_x], 1)
        U_y = tf.stack([u_y, v_y, w_y], 1)
        U_z = tf.stack([u_z, v_z, w_z], 1)

        p_x = tp.gradient(p, x)
        p_y = tp.gradient(p, y)
        p_z = tp.gradient(p, z)

        u_t = tp.gradient(u, t)
        v_t = tp.gradient(v, t)
        w_t = tp.gradient(w, t)

        U_t = tf.stack([u_t, v_t, w_t], 1)

        u_xx = tp.gradient(u_x, x)
        v_xx = tp.gradient(v_x, x)
        w_xx = tp.gradient(w_x, x)

        U_xx = tf.stack([u_xx, v_xx, w_xx], 1)

        u_yy = tp.gradient(u_y, y)
        v_yy = tp.gradient(v_y, y)
        w_yy = tp.gradient(w_y, y)

        U_yy = tf.stack([u_yy, v_yy, w_yy], 1)

        u_zz = tp.gradient(u_z, z)
        v_zz = tp.gradient(v_z, z)
        w_zz = tp.gradient(w_z, z)

        U_zz = tf.stack([u_zz, v_zz, w_zz], 1)
        # rho_x = tf.stack([rho_xx_x, rho_xy_y, rho_xz_z], axis=1)
        # rho_y = tf.stack([rho_yx_x, rho_yy_y, rho_yz_z], axis=1)
        # rho_z = tf.stack([rho_zx_x, rho_zy_y, rho_zz_z], axis=1)
        # rho = tf.stack([rho_x, rho_y, rho_z], axis=1)
        del tp
        # return U, U_t, U_x, U_y, U_z, p, p_x, p_y, p_z, rho
        return U, U_t, U_x, U_xx, U_y, U_yy, U_z, U_zz, p, p_x, p_y, p_z

    def pde(self, x: tf.Tensor, y: tf.Tensor, z: tf.Tensor, t: tf.Tensor) -> Tuple[tf.Tensor, tf.Tensor, tf.Tensor,
                                                                                   tf.Tensor]:
        U, U_t, U_x, U_xx, U_y, U_yy, U_z, U_zz, _, p_x, p_y, p_z = self.gradients(x, y, z, t)
        # U, U_t, U_x, U_y, U_z, _, p_x, p_y, p_z, rho = self.gradients(x, y, z, t)

        mu = 3.5e-3
        p0 = 994
        if U.shape[0] > 0:
            e1 = (U_t[:, 0] + (U[:, 0] * U_x[:, 0] + U[:, 1] * U_y[:, 0] + U[:, 2] * U_z[:, 0]) + p_x
                  # - (rho[:, 0, 0] + rho[:, 0, 1] + rho[:, 0, 2]) / p0)
                  - mu / p0 * (U_xx[:, 0] + U_yy[:, 0] + U_zz[:, 0]))
            e2 = (U_t[:, 1] + (U[:, 0] * U_x[:, 1] + U[:, 1] * U_y[:, 1] + U[:, 2] * U_z[:, 1]) + p_y
                  # - (rho[:, 1, 0] + rho[:, 1, 1] + rho[:, 1, 2]) / p0)
                  - mu / p0 * (U_xx[:, 1] + U_yy[:, 1] + U_zz[:, 1]))
            e3 = (U_t[:, 2] + (U[:, 0] * U_x[:, 2] + U[:, 1] * U_y[:, 2] + U[:, 2] * U_z[:, 2]) + p_z
                  # - (rho[:, 2, 0] + rho[:, 2, 1] + rho[:, 2, 2]) / p0)
                  - mu / p0 * (U_xx[:, 2] + U_yy[:, 2] + U_zz[:, 2]))
            e4 = U_x[:, 0] + U_y[:, 1] + U_z[:, 2]
        else:
            e1 = tf.zeros(1, dtype='float32')
            e2 = tf.zeros(1, dtype='float32')
            e3 = tf.zeros(1, dtype='float32')
            e4 = tf.zeros(1, dtype='float32')
        return e1, e2, e3, e4

    def loss_prediction(self, x: tf.Tensor, y: tf.Tensor, z: tf.Tensor, t: tf.Tensor, U: tf.Tensor,
                        p: tf.Tensor) -> tf.Tensor:
        F = self.net(tf.stack([x, y, z, t], 1))
        U_hat = F[:, :3]
        p_hat = F[:, 3]
        return tf.reduce_sum(tf.reduce_mean(tf.square(U - U_hat), axis=0)) + tf.reduce_mean(tf.square(p - p_hat))

    def loss_boundary(self, x: tf.Tensor, y: tf.Tensor, z: tf.Tensor, t: tf.Tensor) -> tf.Tensor:
        F = self.net(tf.stack([x, y, z, t], 1))
        return tf.reduce_sum(tf.reduce_mean(tf.square(F[:, :3]), axis=0))

    def loss_residual(self, x: tf.Tensor, y: tf.Tensor, z: tf.Tensor, t: tf.Tensor) -> tf.Tensor:
        e1, e2, e3, e4 = self.pde(x, y, z, t)
        return tf.reduce_mean(tf.square(e1)) + tf.reduce_mean(tf.square(e2)) + tf.reduce_mean(tf.square(e3)) + tf.reduce_mean(tf.square(e4))

    @tf.function
    def loss_global(self, x_data: tf.Tensor, y_data: tf.Tensor, z_data: tf.Tensor, t_data: tf.Tensor, U_data: tf.Tensor,
                    p_data: tf.Tensor, x_residual: tf.Tensor, y_residual: tf.Tensor, z_residual: tf.Tensor,
                    t_residual: tf.Tensor, x_boundary: tf.Tensor, y_boundary: tf.Tensor,
                    z_boundary: tf.Tensor, t_boundary: tf.Tensor) -> Tuple[tf.Tensor, tf.Tensor, tf.Tensor, tf.Tensor]:
        loss_prediction = self.loss_prediction(x_data, y_data, z_data, t_data, U_data, p_data)
        if x_boundary.shape[0] > 0:
            loss_boundary = self.loss_boundary(x_boundary, y_boundary, z_boundary, t_boundary)
        else:
            loss_boundary = tf.convert_to_tensor(0, dtype='float32')
        if self.weight_residual == 0.0:
            loss_residual = tf.convert_to_tensor(0, dtype='float32')
        else:
            loss_residual = self.loss_residual(x_residual, y_residual, z_residual, t_residual)
        loss = self.weight_data * loss_prediction + self.weight_residual * loss_residual + self.weight_boundary * loss_boundary
        return loss, loss_prediction, loss_residual, loss_boundary

    def loss_gradient(self, x_data: tf.Tensor, y_data: tf.Tensor, z_data: tf.Tensor, t_data: tf.Tensor,
                      U_data: tf.Tensor, p_data: tf.Tensor, x_residual: tf.Tensor, y_residual: tf.Tensor,
                      z_residual: tf.Tensor, t_residual: tf.Tensor, x_boundary: tf.Tensor, y_boundary: tf.Tensor,
                      z_boundary: tf.Tensor, t_boundary: tf.Tensor) \
            -> Tuple[tf.Tensor, tf.Tensor, tf.Tensor, tf.Tensor, tf.Tensor]:
        with tf.GradientTape(persistent=True) as tp:
            loss, loss_prediction, loss_residual, loss_boundary = self.loss_global(x_data, y_data, z_data, t_data,
                                                                                   U_data, p_data,
                                                                                   x_residual, y_residual, z_residual,
                                                                                   t_residual, x_boundary, y_boundary,
                                                                                   z_boundary, t_boundary)

        grad = tp.gradient(loss, self.net.trainable_variables)
        del tp
        return grad, loss, loss_prediction, loss_residual, loss_boundary

    @tf.function
    def gradient_descent(self, x_data: tf.Tensor, y_data: tf.Tensor, z_data: tf.Tensor, t_data: tf.Tensor,
                         U_data: tf.Tensor, p_data: tf.Tensor, x_residual: tf.Tensor, y_residual: tf.Tensor,
                         z_residual: tf.Tensor, t_residual: tf.Tensor, x_boundary: tf.Tensor, y_boundary: tf.Tensor,
                         z_boundary: tf.Tensor, t_boundary: tf.Tensor):
        grad, loss, loss_prediction, loss_residual, loss_boundary = self.loss_gradient(x_data, y_data, z_data, t_data,
                                                                                       U_data, p_data, x_residual,
                                                                                       y_residual, z_residual,
                                                                                       t_residual, x_boundary,
                                                                                       y_boundary, z_boundary,
                                                                                       t_boundary)
        self.optimizer.apply_gradients(zip(grad, self.net.trainable_variables))
        return loss, loss_prediction, loss_residual, loss_boundary

    def train(self, dataset_data: tf.data.Dataset, dataset_residual: tf.data.Dataset,
              dataset_boundary: tf.data.Dataset = None, epoch=10 ** 2, tol=1e-5):
        self.logger.log_train_start(self.net)
        n_batches = dataset_data.cardinality().numpy()
        if dataset_residual.cardinality().numpy() == 0:
            dataset_residual = n_batches * [tf.zeros((0, 4))]
        if dataset_boundary is None:
            dataset_boundary = n_batches * [tf.zeros((0, 4))]
        for ep in range(epoch):
            ep_loss = 0.0
            ep_loss_data = 0.0
            ep_loss_residual = 0.0
            ep_loss_boundary = 0.0
            for batch_data, batch_residual, batch_boundary in zip(dataset_data, dataset_residual, dataset_boundary):
                x_data = batch_data[:, 0]
                y_data = batch_data[:, 1]
                z_data = batch_data[:, 2]
                t_data = batch_data[:, 3]
                U_data = batch_data[:, 4:7]
                p_data = batch_data[:, 7]

                x_residual = batch_residual[:, 0]
                y_residual = batch_residual[:, 1]
                z_residual = batch_residual[:, 2]
                t_residual = batch_residual[:, 3]

                x_boundary = batch_boundary[:, 0]
                y_boundary = batch_boundary[:, 1]
                z_boundary = batch_boundary[:, 2]
                t_boundary = batch_boundary[:, 3]

                loss_batch, loss_prediction, loss_residual, loss_boundary = self.gradient_descent(x_data, y_data,
                                                                                                  z_data, t_data,
                                                                                                  U_data, p_data,
                                                                                                  x_residual,
                                                                                                  y_residual,
                                                                                                  z_residual,
                                                                                                  t_residual,
                                                                                                  x_boundary,
                                                                                                  y_boundary,
                                                                                                  z_boundary,
                                                                                                  t_boundary)
                ep_loss += loss_batch / n_batches
                ep_loss_data += loss_prediction / n_batches
                ep_loss_residual += loss_residual / n_batches
                ep_loss_boundary += loss_boundary / n_batches

            self.logger.log_epoch(ep_loss, ep_loss_data, ep_loss_residual, ep_loss_boundary)
            if ep_loss < tol:
                break

    def predict(self, dataset: tf.data.Dataset):
        mu = 3.5e-3
        U_p = tf.zeros((0, 3), dtype='float32')
        U_tr = tf.zeros((0, 3), dtype='float32')
        U_X_p = tf.zeros((0, 3, 4), dtype='float32')
        U_XX_p = tf.zeros((0, 3, 3), dtype='float32')
        # rho_p = tf.zeros((0, 3, 3), dtype='float32')
        p_p = tf.zeros((0), dtype='float32')
        p_t = tf.zeros((0), dtype='float32')
        p_X_p = tf.zeros((0, 3), dtype='float32')
        X_ = tf.zeros((0, 4), dtype='float32')
        # wss_ = tf.zeros((0, 3), dtype='float32')
        # wss_p = tf.zeros((0, 3), dtype='float32')
        # normals_ = tf.zeros((0, 3), dtype='float32')
        for batch in dataset:
            x = batch[:, 0]
            y = batch[:, 1]
            z = batch[:, 2]
            t = batch[:, 3]
            U, U_t, U_x, U_xx, U_y, U_yy, U_z, U_zz, p, p_x, p_y, p_z = self.gradients(x, y, z, t)
            # U, U_t, U_x, U_y, U_z, p, p_x, p_y, p_z, rho = self.gradients(x, y, z, t)
            U_X_ = tf.stack([U_x, U_y, U_z, U_t], axis=2)
            p_X_ = tf.stack([p_x, p_y, p_z], axis=1)
            U_XX_ = tf.stack([U_xx, U_yy, U_zz], axis=2)
            U_p = tf.concat([U_p, U], axis=0)
            U_tr = tf.concat([U_tr, batch[:, 4:7]], axis=0)
            U_X_p = tf.concat([U_X_p, U_X_], axis=0)
            # rho_p = tf.concat([rho_p, rho], axis=0)
            U_XX_p = tf.concat([U_XX_p, U_XX_], axis=0)
            p_p = tf.concat([p_p, p], axis=0)
            p_t = tf.concat([p_t, batch[:, 7]], axis=0)
            p_X_p = tf.concat([p_X_p, p_X_], axis=0)
            X_ = tf.concat([X_, batch[:, 0:4]], axis=0)
            # normals_ = tf.concat([normals_, batch[:, 4:7]], axis=0)
            # wss_ = tf.concat([wss_, batch[:, 7:10]], axis=0)
            u = U_X_[:, 0, 0:3]
            v = U_X_[:, 1, 0:3]
            w = U_X_[:, 2, 0:3]
            # wss = tf.stack([tf.math.reduce_sum(u * batch[:, 4:7], axis=-1),
            #                 tf.math.reduce_sum(v * batch[:, 4:7], axis=-1),
            #                 tf.math.reduce_sum(w * batch[:, 4:7], axis=-1)], axis=-1)
            # wss_p = tf.concat([wss_p, wss * mu], axis=0)
        error = tf.reduce_sum(tf.reduce_mean(tf.square(U_p - U_tr), axis=(0, 1))) + \
                tf.reduce_mean(tf.square(p_p - p_t))
        self.logger.log_train_end(error)
        uniques, _ = tf.unique(X_[:, 3])
        T = uniques.shape[0]
        X_ = tf.reshape(X_, (T, -1, 4))
        U_p = tf.reshape(U_p, (T, -1, 3))
        U_tr = tf.reshape(U_tr, (T, -1, 3))
        U_X_p = tf.reshape(U_X_p, (T, -1, 3, 4))
        # rho_p = tf.reshape(rho_p, (T, -1, 3, 3))
        U_XX_p = tf.reshape(U_XX_p, (T, -1, 3, 3))
        p_p = tf.reshape(p_p, (T, -1))
        p_t = tf.reshape(p_t, (T, -1))
        p_X_p = tf.reshape(p_X_p, (T, -1, 3))
        # wss_p = tf.reshape(wss_p, (T, -1, 3))
        # wss_ = tf.reshape(wss_, (T, -1, 3))

        # return X_.numpy(), U_p.numpy(), U_tr.numpy(), U_X_p.numpy(), rho_p.numpy(), \
        #     p_p.numpy(), p_t.numpy(), p_X_p.numpy()  # , wss_p.numpy(), wss_.numpy()

        return X_.numpy(), U_p.numpy(), U_tr.numpy(), U_X_p.numpy(), U_XX_p.numpy(), \
            p_p.numpy(), p_t.numpy(), p_X_p.numpy()  # , wss_p.numpy(), wss_.numpy()


if __name__ == '__main__':
    parameters = get_parameters()
    file_train = os.path.join(parameters.path, 'data/numpy', parameters.case_name,
                              '{}.npz'.format(parameters.train_set_name))
    file_test = os.path.join(parameters.path, 'data/numpy', parameters.case_name,
                             '{}.npz'.format(parameters.test_set_name))

    tf.random.set_seed(seed=parameters.seed)

    ds_data, ds_res, ds_bnd, lower_bound, upper_bound, flags = get_dataset(data_file=file_train,
                                                                           residual_file=file_test,
                                                                           t_frac=parameters.training_fraction,
                                                                           bnd_frac=parameters.boundary_fraction,
                                                                           res_frac=parameters.residual_fraction,
                                                                           seed=parameters.seed)

    ds_data, ds_res, ds_bnd = shuffle_and_batch(data=ds_data, residuals=ds_res, boundary=ds_bnd,
                                                b_size=parameters.batch_size)

    pinn = PINN(params=parameters,
                lb=lower_bound,
                ub=upper_bound)

    pinn.train(dataset_data=ds_data, dataset_residual=ds_res, dataset_boundary=ds_bnd, epoch=parameters.max_epochs)

    ds = get_eval_dataset(file=file_test)
    ds = ds.batch(batch_size=parameters.batch_size)

    # X, U_pred, U_truth, U_X, U_XX, p_pred, p_truth, p_X, wss_pred, wss_truth = pinn.predict(dataset=ds)
    X, U_pred, U_truth, U_X, U_XX, p_pred, p_truth, p_X = pinn.predict(dataset=ds)

    file = os.path.join(parameters.path, 'results', parameters.id)

    np.savez(file=file, X=X, U_truth=U_truth, U_pred=U_pred, U_X=U_X, U_XX=U_XX,
             p_truth=p_truth, p_pred=p_pred, p_X=p_X,
             # wss_pred=wss_pred, wss_truth=wss_truth,
             flags=flags)
