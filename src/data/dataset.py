import time
from time import sleep
import numpy as np
import tensorflow as tf

from math import ceil


def get_dataset(data_file: str, t_frac: float, bnd_frac: float, res_frac: float, seed: int = 1234,
                residual_file: str = None):
    rng = np.random.default_rng(seed)
    data = np.load(data_file)
    X = data['X'].astype('float32')
    N_boundary = data['wss'].shape[1]
    lb = np.min(X, axis=(0, 1))
    ub = np.max(X, axis=(0, 1))
    F = data['F']
    N = X.shape[1]

    N_internal = N - N_boundary
    N_train_internal = round(N_internal * t_frac)
    idx = np.arange(N_internal)
    rng.shuffle(idx)
    data_idx = idx[:N_train_internal]

    if residual_file is None:
        X_ = X
        N_train_bnd = round(N_boundary * bnd_frac)
        idx = np.arange(N - N_boundary, N)
        rng.shuffle(idx)
        bnd_idx = idx[:N_train_bnd]
        data_idx = np.concatenate([data_idx, bnd_idx])
        res_idx = np.arange(N)
        X_res = X.reshape((-1, 4))
        bnd_dataset = None
        bnd_idx = np.zeros((0,), dtype='int32')
    else:
        data = np.load(residual_file)
        X_ = data['X']
        N = X_.shape[1]
        N_boundary = data['normals'].shape[0]
        N_res = round(N * res_frac)
        idx = np.arange(N)
        rng.shuffle(idx)
        res_idx = idx[:N_res]
        X_res = X_[:, res_idx, :].reshape(-1, 4)
        N_bnd = round(N_boundary * bnd_frac)
        idx = np.arange(N - N_boundary, N)
        rng.shuffle(idx)
        bnd_idx = idx[:N_bnd]
        X_bnd = X_[:, bnd_idx, :].reshape((-1, 4))
        bnd_dataset = tf.data.Dataset.from_tensor_slices(tf.convert_to_tensor(X_bnd, dtype='float32'))

    X_data = X[:, data_idx, :].reshape((-1, 4))
    F_data = F[:, data_idx, :].reshape((-1, 4))
    flags = np.zeros(N)
    data_res_mapping = np.zeros_like(data_idx)
    count = 0
    for i, idx in enumerate(data_idx):
        new_idx = np.argwhere(np.all(X[0, idx, :3] == X_[0, :, :3], axis=-1)).reshape(-1)
        assert new_idx.shape[0] == 1
        data_res_mapping[i] = new_idx[0]
    flags[res_idx] = 3
    flags[bnd_idx] = 2
    flags[data_res_mapping] = 1

    return (tf.data.Dataset.from_tensor_slices(tf.convert_to_tensor(np.concatenate([X_data, F_data], axis=-1),
                                                                    dtype='float32')),
            tf.data.Dataset.from_tensor_slices(tf.convert_to_tensor(X_res, dtype='float32')),
            bnd_dataset, lb, ub,
            flags)


def get_eval_dataset(file: str):
    data = np.load(file)
    X = data['X']
    F = data['F']
    # normals = data['normals']
    wss = data['wss']

    wss = np.concatenate([np.zeros((wss.shape[0], X.shape[1] - wss.shape[1], wss.shape[2])), wss], axis=1)
    # normals = np.concatenate([np.zeros((X.shape[1] - normals.shape[0], normals.shape[1])), normals], axis=0)
    # normals = np.tile(normals, (X.shape[0], 1, 1))
    # normals = normals.reshape((-1, 3))
    wss = wss.reshape((-1, 3))
    X = X.reshape((-1, 4))
    F = F.reshape((-1, 4))
    # return tf.data.Dataset.from_tensor_slices(tf.convert_to_tensor(np.concatenate([X, F, normals, wss], axis=-1),
    return tf.data.Dataset.from_tensor_slices(tf.convert_to_tensor(np.concatenate([X, F], axis=-1),
                                                                   dtype='float32'))


def shuffle_and_batch(data: tf.data.Dataset, residuals: tf.data.Dataset, boundary: tf.data.Dataset, b_size: int):
    data_card = data.cardinality().numpy()
    res_card = residuals.cardinality().numpy()
    bnd_card = boundary.cardinality().numpy() if boundary is not None else 0
    if res_card > 0:
        residuals = residuals.shuffle(residuals.cardinality(), seed=1234)
        residuals = residuals.batch(batch_size=b_size)
        b_size = ceil(b_size * data_card / res_card) if data_card > bnd_card \
            else ceil(b_size * bnd_card / res_card)
    if bnd_card > data_card:
        boundary = boundary.shuffle(boundary.cardinality(), seed=1234)
        boundary = boundary.batch(batch_size=b_size)
        b_size = ceil(b_size * data_card / bnd_card)
        data = data.shuffle(data.cardinality(), seed=5678)
        data = data.batch(batch_size=b_size)
    else:
        data = data.shuffle(data.cardinality(), seed=5678)
        data = data.batch(batch_size=b_size)
        if bnd_card > 0:
            b_size = ceil(b_size * bnd_card / data_card)
            boundary = boundary.shuffle(boundary.cardinality(), seed=1234)
            boundary = boundary.batch(batch_size=b_size)

    return data, residuals, boundary


if __name__ == '__main__':
    ds = get_eval_dataset(file='/home/moritz/Documents/deepflow/data/numpy/cfd0/partial_4575_11.npz')
    batch_size = 5000
    training_fraction = 1.0
    boundary_fraction = 1.0
    residual_fraction = 1.0
    ds_data, ds_res, ds_bnd, lb, ub, data_flag = get_dataset(
        data_file='/home/moritz/Documents/deepflow/data/numpy/cfd0/partial_4575_11.npz',
        # residual_file='/home/moritz/Documents/deepflow/data/numpy/cfd0/partial_4575_11.npz',
        t_frac=training_fraction,
        bnd_frac=boundary_fraction,
        res_frac=residual_fraction
    )
    ds_data, ds_res, ds_bnd = shuffle_and_batch(data=ds_data, residuals=ds_res, boundary=ds_bnd, b_size=batch_size)
    data_card = ds_data.cardinality().numpy()
    res_card = ds_res.cardinality().numpy()
    bnd_card = ds_bnd.cardinality().numpy() if ds_bnd is not None else 0
    if bnd_card > 0:
        assert data_card == res_card == bnd_card, 'The datasets should have the same cardinality, ({}, {}, {})'.format(
            data_card, res_card, bnd_card)
        start = time.perf_counter()
        for batch_data, batch_residual, batch_boundary in zip(ds_data, ds_res, ds_bnd):
            offset = round(np.random.random() * (batch_data.shape[0] - 10))
            selection = batch_data[offset:offset + 10, :]
            sleep(0.1)
    else:
        assert data_card == res_card, 'The datasets should have the same cardinality, ({}, {})'.format(data_card,
                                                                                                       res_card)
        start = time.perf_counter()
        for batch_data, batch_residual in zip(ds_data, ds_res):
            sleep(0.1)
    exec_time = time.perf_counter() - start - 0.1 * data_card
    print('Execution time: ', exec_time)
    print('Per batch:      ', exec_time / data_card)
