import os
import numpy as np
import tensorflow as tf
import tensorflow_gnn as tfgnn
from tensorflow_gnn import runner

import time
from time import sleep


def get_dataset_for_graph(file: str, t_frac: float, seed: int = 1234):
    graph_schema_pbtxt = """
    node_sets {
      key: "nodes"
      value {
        description: "Nodes"

        features {
          key: "F"
          value: {
            description: "[U, p]"
            dtype: DT_FLOAT
            shape { dim { size: 4 } }
          }
        }

        features {
          key: "x"
          value: {
            description: "x"
            dtype: DT_FLOAT
            shape { dim { size: 1 } }
          }
        }
        
        features {
          key: "y"
          value: {
            description: "y"
            dtype: DT_FLOAT
            shape { dim { size: 1 } }
          }
        }
        
        features {
          key: "z"
          value: {
            description: "z"
            dtype: DT_FLOAT
            shape { dim { size: 1 } }
          }
        }
        
        features {
          key: "t"
          value: {
            description: "t"
            dtype: DT_FLOAT
            shape { dim { size: 1 } }
          }
        }

        features {
          key: "normals"
          value: {
            description: "normals"
            dtype: DT_FLOAT
            shape { dim { size: 3 } }
          }
        }

        features {
          key: "wss"
          value: {
            description: "wss"
            dtype: DT_FLOAT
            shape { dim { size: 3 } }
          }
        }
      }
    }

    edge_sets {
      key: "edges"
      value {
        description: "Edges"
        source: "nodes"
        target: "nodes"
      }
    }
    """
    graph_schema = tfgnn.parse_schema(graph_schema_pbtxt)
    graph_spec = tfgnn.create_graph_spec_from_schema_pb(graph_schema)
    rng = np.random.default_rng(seed)
    data = np.load(file)
    X = data['X']
    F = data['F']
    lb = np.min(X, axis=(0, 1))
    ub = np.min(X, axis=(0, 1))
    edge_index = data['edge_index']
    N = X.shape[1]
    N_train = round(N * t_frac)
    idx = np.arange(N)
    rng.shuffle(idx)
    data_idx = idx[:N_train]
    normals = data['normals']
    wss = data['wss']
    wss = np.concatenate([np.zeros((wss.shape[0], X.shape[1] - wss.shape[1], wss.shape[2])), wss], axis=1)
    normals = np.concatenate([np.zeros((X.shape[1] - normals.shape[0], normals.shape[1])), normals], axis=0)

    base_dir = os.path.join(os.path.dirname(file), 'tfrecords')
    try:
        os.mkdir(os.path.join(base_dir))
    except FileExistsError:
        pass
    tf_records_data_file = os.path.join(base_dir, '{}.{}.tfrecords'.format(os.path.basename(file), t_frac))
    if os.path.isfile(tf_records_data_file):
        os.remove(tf_records_data_file)
    with tf.io.TFRecordWriter(tf_records_data_file) as writer:
        for t in range(X.shape[0]):
            graph = tfgnn.GraphTensor.from_pieces(
                node_sets={'nodes': tfgnn.NodeSet.from_fields(
                    sizes=tf.constant([X.shape[1]]),
                    features={
                        'x': tf.constant(X[t, :, 0]),
                        'y': tf.constant(X[t, :, 1]),
                        'z': tf.constant(X[t, :, 2]),
                        't': tf.constant(X[t, :, 3]),
                        'F': tf.constant(F[t, :, :]),
                        'normals': tf.constant(normals),
                        'wss': tf.constant(wss[t, :, :])
                    }
                )},
                edge_sets={'edges': tfgnn.EdgeSet.from_fields(
                    sizes=tf.constant([edge_index.shape[1]]),
                    adjacency=tfgnn.Adjacency.from_indices(
                        source=('nodes', tf.constant([edge_index[0, :]])),
                        target=('nodes', tf.constant([edge_index[1, :]]))
                    ))})
            example = tfgnn.write_example(graph)
            writer.write(example.SerializeToString())

    data_provider = runner.TFRecordDatasetProvider(file_pattern=tf_records_data_file)
    ds_d = data_provider.get_dataset(tf.distribute.InputContext())
    ds_d = ds_d.map(lambda serialized: tfgnn.parse_single_example(serialized=serialized, spec=graph_spec))
    ds_r = data_provider.get_dataset(tf.distribute.InputContext())
    ds_r = ds_r.map(lambda serialized: tfgnn.parse_single_example(serialized=serialized, spec=graph_spec))
    ds_eval = data_provider.get_dataset(tf.distribute.InputContext())
    ds_eval = ds_eval.map(lambda serialized: tfgnn.parse_single_example(serialized=serialized, spec=graph_spec))
    return ds_d, ds_r, ds_eval, lb, ub, data_idx, graph_spec


def shuffle(data: tf.data.Dataset, residuals: tf.data.Dataset):
    if residuals.cardinality().numpy() > 0:
        residuals = residuals.shuffle(residuals.cardinality(), seed=1234)
    data = data.shuffle(data.cardinality(), seed=5678)
    return data, residuals


if __name__ == '__main__':
    batch_size = 512
    training_fraction = 0.1

    ds_data, ds_res, ds, lb, ub, d_idx, _ = get_dataset_for_graph(file='/home/moritz/Documents/deepflow/data/numpy/cfd0'
                                                                       '/partial_4318_11.npz',
                                                                  t_frac=training_fraction,
                                                                  seed=1234)

    ds_data, ds_res = shuffle(data=ds_data, residuals=ds_res)
    data_card = ds_data.cardinality().numpy()
    res_card = ds_res.cardinality().numpy()
    assert data_card == res_card, 'The datasets should have the same cardinality, ({}, {})'.format(data_card, res_card)
    start = time.perf_counter()
    for batch_data, batch_residual in zip(ds_data, ds_res):
        print(batch_data.node_sets['nodes'].features['x'])
        print(batch_data.node_sets['nodes'].features['F'])
        sleep(0.1)
    exec_time = time.perf_counter() - start - 0.1 * data_card
    print('Execution time: ', exec_time)
    print('Per batch:      ', exec_time / data_card)
