import os

import vtk
from vtk.util.numpy_support import vtk_to_numpy
from vtk import vtkOpenFOAMReader, vtkDoubleArray, vtkPoints, vtkPlanes, vtkClipDataSet

from typing import List, Tuple, Dict

import fluidfoam
import numpy as np


def preprocess_vtk(path: str, name: str):
    u_scale = 1e-0
    data_file = os.path.join(path, 'foam', name, 'foam.foam')
    reader = vtkOpenFOAMReader()
    reader.SetFileName(data_file)
    reader.SkipZeroTimeOn()
    reader.SetPatchArrayStatus('patch/wall', 1)
    reader.Update()

    output: vtk.vtkMultiBlockDataSet = reader.GetOutput()
    internal: vtk.vtkUnstructuredGrid = output.GetBlock(0)
    wall: vtk.vtkPolyData = output.GetBlock(1).GetBlock(0)

    wall_points: np.ndarray = vtk_to_numpy(wall.GetPoints().GetData())
    internal_points: np.ndarray = vtk_to_numpy(internal.GetPoints().GetData())

    wall_condition = np.all([wall_points[:, 1] <= 0.033, wall_points[:, 1] >= 0.03], axis=0)
    internal_condition = np.all([internal_points[:, 1] <= 0.033, internal_points[:, 1] >= 0.03], axis=0)

    wall_selection = np.nonzero(wall_condition)[0]
    internal_selection = np.nonzero(internal_condition)[0]

    internal_points_selection = internal_points[internal_selection]
    to_drop = []
    for i, idx in enumerate(wall_selection):
        duplicate_idx = np.nonzero(np.all(internal_points_selection == wall_points[idx, :], axis=-1))[0]
        assert duplicate_idx.shape[0] == 1
        to_drop.append(duplicate_idx[0])
    mask = np.ones_like(internal_selection)
    mask[to_drop] = 0
    internal_selection = internal_selection[np.nonzero(mask)[0]]

    X = np.concatenate([internal_points[internal_selection, :], wall_points[wall_selection, :]]).reshape((1, -1, 3))
    F = np.empty((0, internal_selection.shape[0] + wall_selection.shape[0], 4))
    wss = np.empty((0, wall_selection.shape[0], 3))

    time_values: vtkDoubleArray = reader.GetTimeValues()
    T = []
    for i in range(time_values.GetNumberOfTuples()):
        t = time_values.GetTuple(i)[0]
        if 2.0 <= t <= 3.0:
            T.append(t)
            reader.UpdateTimeStep(t)
            output: vtk.vtkMultiBlockDataSet = reader.GetOutput()
            internal: vtk.vtkUnstructuredGrid = output.GetBlock(0)
            wall: vtk.vtkPolyData = output.GetBlock(1).GetBlock(0)

            wall_point_data: vtk.vtkPointData = wall.GetPointData()
            internal_point_data: vtk.vtkPointData = internal.GetPointData()
            wall_U = vtk_to_numpy(wall_point_data.GetVectors('U'))[wall_selection, :] * u_scale
            wall_p = vtk_to_numpy(wall_point_data.GetScalars('p'))[wall_selection]
            internal_U = vtk_to_numpy(internal_point_data.GetVectors('U'))[internal_selection, :] * u_scale
            internal_p = vtk_to_numpy(internal_point_data.GetVectors('p'))[internal_selection]
            p_tmp = np.concatenate([internal_p, wall_p]).reshape((-1, 1))
            U_tmp = np.concatenate([internal_U, wall_U], axis=0)
            F = np.concatenate([F, np.concatenate([U_tmp, p_tmp], axis=-1).reshape((1, -1, 4))], axis=0)
            wss_tmp = vtk_to_numpy(wall_point_data.GetVectors('wallShearStress'))[wall_selection, :].reshape((1, -1, 3))
            wss = np.concatenate([wss, wss_tmp], axis=0)
    T = np.array(T).reshape(-1, 1, 1)
    T = np.tile(T, (1, X.shape[1], 1))
    X = np.tile(X, (T.shape[0], 1, 1))
    X = np.concatenate([X, T], axis=-1)
    N = X.shape[1]
    T = X.shape[0]
    try:
        os.mkdir(os.path.join(path, 'numpy', name))
    except FileExistsError:
        pass
    save_file = os.path.join(path, 'numpy', name, 'partial_{}_{}.npz'.format(N, T))
    print('writing to {}'.format(save_file))
    try:
        os.remove(save_file)
    except FileNotFoundError:
        pass

    np.savez(file=save_file,
             X=X, F=F, wss=wss)


def sanity_check(path: str) -> None:
    polyMeshPath = os.path.join(path, 'constant/polyMesh')
    assert os.path.isdir(path), '{} is not a directory.'.format(path)
    assert os.path.isfile(os.path.join(polyMeshPath, 'owner')), \
        'expected to find \'owner\' in \'{}\''.format(polyMeshPath)
    assert os.path.isfile(os.path.join(polyMeshPath, 'faces')), \
        'expected to find \'faces\' in \'{}\''.format(polyMeshPath)
    assert os.path.isfile(os.path.join(polyMeshPath, 'points')), \
        'expected to find \'points\' in \'{}\''.format(polyMeshPath)
    assert os.path.isfile(os.path.join(polyMeshPath, 'neighbour')), \
        'expected to find \'neighbour\' in \'{}\''.format(polyMeshPath)
    assert os.path.isfile(os.path.join(polyMeshPath, 'points')), \
        'expected to find \'points\' in \'{}\''.format(polyMeshPath)
    assert os.path.isfile(os.path.join(polyMeshPath, 'boundary')), \
        'expected to find \'boundary\' in \'{}\''.format(polyMeshPath)


def read_file(path: str) -> List[str]:
    with open(path, 'r') as file:
        return [line.replace('\n', '').replace(';', '').strip() for line in file]


def get_points(path: str) -> Tuple[np.ndarray, List[str], int]:

    t_labels = [t for t in os.listdir(path)
                if t not in ['0', '0.orig', 'constant', 'parameters', 'foam.foam']]
    t = np.sort(np.array([float(t) for t in t_labels])).astype('float32')

    x_i, y_i, z_i = fluidfoam.readof.readmesh(path, verbose=False)
    x_b, y_b, z_b = fluidfoam.readof.readmesh(path, boundary='wall', verbose=False)

    x = np.concatenate([x_i, x_b]).astype('float32')
    y = np.concatenate([y_i, y_b]).astype('float32')
    z = np.concatenate([z_i, z_b]).astype('float32')

    T = t.shape[0]
    N = x.shape[0]

    return np.stack([np.tile(x, (T, 1)),
                     np.tile(y, (T, 1)),
                     np.tile(z, (T, 1)),
                     np.tile(t.reshape((-1, 1)), (1, N))],
                    axis=2), t_labels, x_b.shape[0]


def get_normals(path: str, n_boundary: int) -> np.ndarray:
    lines = read_file(os.path.join(path, 'constant/polyMesh/points'))
    coo_list = lines[lines.index('(')+1:lines.index(')')]
    coo = np.array([[float(i) for i in coo.strip('()').split()]
                    for coo in coo_list])
    lines = read_file(os.path.join(path, 'constant/polyMesh/faces'))
    faces_list = lines[lines.index('(')+1:lines.index(')')][-n_boundary:]
    faces = [[int(i) for i in face[1:].strip('()').split()] for face in faces_list]

    def _calculate_normal(points: List[int]) -> np.ndarray:
        p = [coo[p] for p in points]
        a = p[0] - p[1]
        b = p[2] - p[1]
        n = np.cross(a, b)
        return n / np.sqrt(np.sum(n ** 2))

    return np.array([_calculate_normal(points) for points in faces]).astype('float32')


def get_connectivity(path: str, boundary_offset: int) -> Tuple[np.ndarray, np.ndarray]:
    lines = read_file(os.path.join(path, 'constant/polyMesh/neighbour'))
    lines = lines[lines.index('(')+1:lines.index(')')]
    neighbours = np.array([int(n) for n in lines])

    lines = read_file(os.path.join(path, 'constant/polyMesh/boundary'))
    lines = lines[lines.index('wall'):]
    lines = lines[lines.index('{')+1:lines.index('}')]
    boundary_parameters = [bp.split() for bp in lines]

    n_faces = int(next(iter([p[1] for p in boundary_parameters if p[0] == 'nFaces'])))
    start_face = int(next(iter([p[1] for p in boundary_parameters if p[0] == 'startFace'])))

    lines = read_file(os.path.join(path, 'constant/polyMesh/owner'))
    lines = lines[lines.index('(')+1:lines.index(')')]
    owners = np.array([int(o) for o in lines])

    boundary_owners = owners[start_face:start_face+n_faces]
    boundary_idx = np.arange(n_faces) + boundary_offset

    internal_owners = owners[:neighbours.shape[0]]

    internal_edge_index = np.stack([internal_owners, neighbours], axis=0)
    boundary_edge_index = np.stack([boundary_owners, boundary_idx], axis=0)

    owner_edges_a = np.isin(internal_edge_index[0, :], boundary_owners)
    owner_edges_b = np.isin(internal_edge_index[1, :], boundary_owners)

    owner_edges = internal_edge_index[:, np.where(np.logical_and(owner_edges_a, owner_edges_b))[0]]

    boundary_boundary_edge_index = np.ndarray(owner_edges.shape)
    for i in range(owner_edges.shape[1]):
        i_a = np.where(boundary_owners == owner_edges[0, i])[0][0]
        i_b = np.where(boundary_owners == owner_edges[1, i])[0][0]
        boundary_boundary_edge_index[0, i] = boundary_idx[i_a]
        boundary_boundary_edge_index[1, i] = boundary_idx[i_b]
    print(boundary_boundary_edge_index)

    edge_index = np.concatenate([internal_edge_index, boundary_edge_index, boundary_boundary_edge_index], axis=1)
    return edge_index.astype('int32'), boundary_owners.astype('int32')


def get_data(path: str, t: str, boundary_owners: np.ndarray) -> Tuple[np.ndarray, np.ndarray]:
    lines = read_file(os.path.join(path, t, 'wallShearStress'))
    lines = lines[lines.index('wall'):]
    lines = lines[lines.index('(')+1:lines.index(')')]
    wss = np.array([[float(v) for v in line.strip('()').split()] for line in lines])

    lines = read_file(os.path.join(path, t, 'U'))
    lines = lines[lines.index('(')+1:lines.index(')')]
    U_i = np.array([[float(v) for v in line.strip('()').split()] for line in lines])
    U_b = np.zeros_like(wss)
    U = np.concatenate([U_i, U_b], axis=0)

    lines = read_file(os.path.join(path, t, 'p'))
    lines = lines[lines.index('(') + 1:lines.index(')')]
    p_i = np.array([[float(v) for v in line.strip('()').split()] for line in lines]).flatten()
    p_b = p_i[boundary_owners]
    p = np.concatenate([p_i, p_b]).reshape((-1, 1))

    return np.concatenate([U, p], axis=1).astype(dtype='float32'), wss.astype('float32')


def parse_folder(path: str, name: str) -> None:

    foam_path = os.path.join(path, 'foam', name)
    sanity_check(foam_path)

    print()
    print('####################################')
    print('#####         Dataset          #####')
    print('####################################')
    print()
    print('path: {}'.format(foam_path))
    print()

    X, t_labels, n_boundary = get_points(foam_path)
    N = X.shape[1]
    T = X.shape[0]

    print('N   : {} ({})'.format(N, n_boundary))
    print('T   : {}'.format(N))

    edge_index, boundary_owners = get_connectivity(foam_path, N - n_boundary)

    print('E   : {}'.format(edge_index.shape[1]))
    print()

    normals = get_normals(foam_path, n_boundary)

    F = np.ndarray((T, N, 4))
    wss = np.ndarray((T, n_boundary, 3))
    for i, label in enumerate(t_labels):
        F_, wss_ = get_data(foam_path, label, boundary_owners)
        F[i, :, :] = F_
        wss[i, :, :] = wss_

    try:
        os.mkdir(os.path.join(path, 'numpy', name))
    except FileExistsError:
        pass
    save_file = os.path.join(path, 'numpy', name, 'complete_{}_{}.npz'.format(N, T))
    print('writing to {}'.format(save_file))
    try:
        os.remove(save_file)
    except FileNotFoundError:
        pass

    np.savez(file=save_file,
             X=X, F=F, wss=wss,
             edge_index=edge_index,
             normals=normals)


def clip(X: np.ndarray, F: np.ndarray, wss: np.ndarray, normals: np.ndarray, edge_index: np.ndarray,
         bnd: Dict[str, float]) -> Tuple[np.ndarray, np.ndarray, np.ndarray, np.ndarray, np.ndarray]:
    boundary_offset = X.shape[1] - normals.shape[0]

    x_max = bnd['x_max'] if 'x_max' in bnd else float('inf')
    x_min = bnd['x_min'] if 'x_min' in bnd else float('-inf')
    y_max = bnd['y_max'] if 'y_max' in bnd else float('inf')
    y_min = bnd['y_min'] if 'y_min' in bnd else float('-inf')
    z_max = bnd['z_max'] if 'z_max' in bnd else float('inf')
    z_min = bnd['z_min'] if 'z_min' in bnd else float('-inf')
    t_max = bnd['t_max'] if 't_max' in bnd else float('inf')
    t_min = bnd['t_min'] if 't_min' in bnd else float('-inf')

    s_idx_oob = np.unique(np.concatenate([np.nonzero((x_min > X[0, :, 0]) | (X[0, :, 0] > x_max))[0],
                                          np.nonzero((y_min > X[0, :, 1]) | (X[0, :, 1] > y_max))[0],
                                          np.nonzero((z_min > X[0, :, 2]) | (X[0, :, 2] > z_max))[0]]))
    t_idx_oob = np.nonzero((t_min > X[:, 0, 3]) | (X[:, 0, 3] > t_max))

    bnd_idx_oob = s_idx_oob[-np.count_nonzero(s_idx_oob >= boundary_offset):] - boundary_offset
    edge_idx_oob = np.unique(np.nonzero(np.any(np.isin(edge_index, s_idx_oob), axis=0)))
    X = np.delete(np.delete(X, t_idx_oob, axis=0), s_idx_oob, axis=1)
    F = np.delete(np.delete(F, t_idx_oob, axis=0), s_idx_oob, axis=1)
    wss = np.delete(np.delete(wss, t_idx_oob, axis=0), bnd_idx_oob, axis=1)
    normals = np.delete(normals, bnd_idx_oob, axis=0)
    edge_index = np.delete(edge_index, edge_idx_oob, axis=1)

    for i in range(edge_index.shape[1]):
        shift = np.count_nonzero(np.stack([s_idx_oob, s_idx_oob]) < np.reshape(edge_index[:, i], (-1, 1)), axis=1)
        edge_index[:, i] -= shift

    return X, F, wss, normals, edge_index


def preprocess(path, name, bnd):
    folder: str = os.path.join(path, 'numpy', name)
    content = os.listdir(folder)
    try:
        npz_file = next(iter([f for f in content if f[:8] == 'complete']))
    except StopIteration:
        print('no complete dataset at location {}, trying to create from foam files'.format(folder))
        parse_folder(path, name)

    dataset = np.load(os.path.join(folder, npz_file))
    X = dataset['X']
    F = dataset['F']
    wss = dataset['wss']
    normals = dataset['normals']
    edge_index = dataset['edge_index']

    if bnd is not None:
        X, F, wss, normals, edge_index = clip(X, F, wss, normals, edge_index, bnd)

    save_file = os.path.join(folder, 'partial_{}_{}'.format(X.shape[1], X.shape[0]))
    print(save_file)

    try:
        os.remove(save_file)
    except FileNotFoundError:
        pass

    np.savez(file=save_file,
             X=X, F=F, wss=wss,
             normals=normals,
             edge_index=edge_index)


if __name__ == '__main__':
    preprocess_vtk(path='/home/moritz/Documents/deepflow/data',
                   name='cfd0')
    skip = True
    if skip:
        exit()
    preprocess(path='/home/moritz/Documents/deepflow/data',
               name='cfd0',
               bnd={'y_min': 0.03,
                    'y_max': 0.031,
                    't_max': 3.0})
    preprocess(path='/home/moritz/Documents/deepflow/data',
               name='cfd0',
               bnd={'y_min': 0.03,
                    'y_max': 0.033,
                    't_min': 2.0,
                    't_max': 3.0})
    preprocess(path='/home/moritz/Documents/deepflow/data',
               name='cfd0',
               bnd={'y_min': 0.03,
                    'y_max': 0.033})
    preprocess(path='/home/moritz/Documents/deepflow/data',
               name='cfd0',
               bnd={'x_max': 0.053,
                    'y_min': 0.049,
                    'y_max': 0.053,
                    'z_min': 0.046,
                    't_min': 2.0,
                    't_max': 4.0})

    preprocess(path='/home/moritz/Documents/deepflow/data',
               name='cfd0',
               bnd={'x_max': 0.053,
                    'y_min': 0.048,
                    'y_max': 0.053,
                    'z_min': 0.046,
                    't_min': 2.0,
                    't_max': 3.0})
    preprocess(path='/home/moritz/Documents/deepflow/data',
               name='cfd0',
               bnd={'x_max': 0.053,
                    'y_min': 0.048,
                    'y_max': 0.049,
                    'z_min': 0.046,
                    't_min': 2.0,
                    't_max': 3.0})
