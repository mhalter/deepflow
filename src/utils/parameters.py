from argparse import Namespace

default_parameters = Namespace(
    id='local',
    path='/itet-stor/mhalter/net_scratch/deepflow',
    case_name='cfd0',
    train_set_name='partial_4318_11',
    test_set_name='partial_12933_11',
    batch_size=5000,
    learning_rate=1e-3,
    hidden_channels=32,
    depth=8,
    activation='gelu',
    w_data=1000.0,
    w_residual=1.0,
    w_boundary=1.0,
    max_epochs=5000,
    training_fraction=0.1,
    boundary_fraction=0.1,
    residual_fraction=0.5,
    seed=1234,
    batch_norm=False
)