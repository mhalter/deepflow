import tensorflow as tf

import time
from datetime import datetime


class Logger:
    def __init__(self, params):
        self.log_freq = 100
        self.batch = 0
        self.epoch = 0
        self.start = None
        self.prev_time = None
        self.loss = 0.0
        self.loss_data = 0.0
        self.loss_residual = 0.0
        print()
        print("==================")
        print("Parameters:")
        keys = [k for k in vars(params)]
        keys.sort()
        max_key_length = max([len(k) for k in keys])
        for k in keys:
            print(k.ljust(max_key_length), getattr(params, k))
        print()

    def log_tf_details(self):
        print()
        print("==================")
        print("TensorFlow:")
        print("version:          {}".format(tf.__version__))
        print("Eager execution : {}".format(tf.executing_eagerly()))
        print("GPU-accerelated : {}".format(len(tf.config.list_physical_devices('GPU')) > 0))
        print("Physical devices: {}".format(', '.join(device.name
                                                      for device in (tf.config.list_physical_devices('GPU')
                                                                     if len(tf.config.list_physical_devices('GPU')) > 0
                                                                     else tf.config.list_physical_devices('CPU')))))
        print()

    def log_train_start(self, model):
        self.start = time.time()
        self.prev_time = self.start
        print()
        print("==================")
        print(model.summary())
        print()
        print()
        print("Training started")
        print("==================")

    def log_epoch(self, loss, loss_data, loss_residual,  loss_boundary):
        self.epoch += 1
        now = time.time()
        if self.epoch % self.log_freq == 0:
            elapsed = datetime.fromtimestamp(now - self.start).strftime("%H:%M:%S")
            epoch_duration = datetime.fromtimestamp(now - self.prev_time).strftime("%M:%S.%f")[:-5]
            print(f"Epoch: {self.epoch}, "
                  f"Loss: {loss:.3e} (data: {loss_data:.3e}, residual: {loss_residual:.3e}, "
                  f"boundary: {loss_boundary:.3e}),  elapsed: {elapsed} (+{epoch_duration})")
        self.prev_time = now

    def log_train_end(self, error):
        elapsed = datetime.fromtimestamp(time.time() - self.start).strftime("%H:%M:%S")
        print("==================")
        print(f"Training finished (epoch {self.epoch}): "
              f"duration = {elapsed}, error = {error:.3e}")

    @staticmethod
    def log_validation_loss(loss):
        print()
        print(f"Validation loss: {loss:.3e}")
