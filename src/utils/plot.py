import os
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.pyplot import Button

if __name__ == '__main__':
    file = os.path.join('/home/moritz/Documents/deepflow/results',
                        '{}.npz'.format('779092'))
    data = np.load(file=file)

    def_elev = 90  # 110
    def_azim = 0  # 10
    def_roll = 0  # 10

    data_orig = np.load(file='/home/moritz/Documents/deepflow/data/numpy/cfd0/partial_4318_11.npz')
    normals = -data_orig['normals']
    n_boundary = normals.shape[0]
    U_X = data['U_X'][:, -n_boundary:, :, :]
    uu = 2 * U_X[:, :, 0, 0]
    uv = U_X[:, :, 0, 1] + U_X[:, :, 1, 0]
    uw = U_X[:, :, 0, 2] + U_X[:, :, 2, 0]
    vv = 2 * U_X[:, :, 1, 1]
    vw = U_X[:, :, 1, 2] + U_X[:, :, 2, 1]
    ww = 2 * U_X[:, :, 2, 2]
    nx = np.tile(normals[:, 0], (U_X.shape[0], 1))
    ny = np.tile(normals[:, 1], (U_X.shape[0], 1))
    nz = np.tile(normals[:, 2], (U_X.shape[0], 1))

    mu = 3.5e-3

    sx = (uu * nx + uv * ny + uw * nz) * mu
    sy = (uv * nx + vv * ny + vw * nz) * mu
    sz = (uw * nx + vw * ny + ww * nz) * mu
    wss_pred = np.stack([sx, sy, sz], axis=-1)

    X = data['X']
    U_pred = data['U_pred']
    U_truth = data['U_truth']
    wss_truth = np.sqrt(np.sum(np.square(data_orig['wss']), axis=-1))
    wss_pred = np.sqrt(np.sum(np.square(wss_pred), axis=-1))
    p_truth = data['p_truth'] - np.mean(data['p_truth'])
    p_pred = data['p_pred'] - np.mean(data['p_pred'])
    mag_U_pred = np.sqrt(np.sum(np.square(data['U_pred']), axis=-1))
    mag_U_truth = np.sqrt(np.sum(np.square(data['U_truth']), axis=-1))

    # X = X[:, :-n_boundary, :]
    # U_pred = U_pred[:, :-n_boundary, :]
    # U_truth = U_truth[:, :-n_boundary, :]
    # p_truth = p_truth[:, :-n_boundary]
    # p_pred = p_pred[:, :-n_boundary]
    # mag_U_pred = mag_U_pred[:, :-n_boundary]
    # mag_U_truth = mag_U_truth[:, :-n_boundary]

    re_U = np.sqrt(np.mean(np.square(mag_U_pred - mag_U_truth), axis=1)
                   / np.mean(np.square(mag_U_truth - np.mean(mag_U_truth)), axis=1))
    re_u = np.sqrt(np.mean(np.square(U_truth[:, :, 0] - U_pred[:, :, 0]), axis=1)
                   / np.mean(np.square(U_truth[:, :, 0] - np.mean(U_truth[:, :, 0])), axis=1))
    re_v = np.sqrt(np.mean(np.square(U_truth[:, :, 1] - U_pred[:, :, 1]), axis=1)
                   / np.mean(np.square(U_truth[:, :, 1] - np.mean(U_truth[:, :, 1])), axis=1))
    re_w = np.sqrt(np.mean(np.square(U_truth[:, :, 2] - U_pred[:, :, 2]), axis=1)
                   / np.mean(np.square(U_truth[:, :, 2] - np.mean(U_truth[:, :, 2])), axis=1))
    re_p = np.sqrt(np.mean(np.square(p_truth - p_pred), axis=1)
                   / np.mean(np.square(p_truth - np.mean(p_pred)), axis=1))
    re_wss = np.sqrt(np.mean(np.square(wss_pred - wss_truth), axis=1)
                     / np.mean(np.square(wss_truth - np.mean(wss_truth)), axis=1))
    RE_U = np.sqrt(np.mean(np.square(mag_U_pred - mag_U_truth))
                   / np.mean(np.square(mag_U_pred - np.mean(mag_U_pred))))
    RE_u = np.sqrt(np.mean(np.square(U_truth[:, :, 0] - U_pred[:, :, 0]))
                   / np.mean(np.square(U_truth[:, :, 0] - np.mean(U_truth[:, :, 0]))))
    RE_v = np.sqrt(np.mean(np.square(U_truth[:, :, 1] - U_pred[:, :, 1]))
                   / np.mean(np.square(U_truth[:, :, 1] - np.mean(U_truth[:, :, 1]))))
    RE_w = np.sqrt(np.mean(np.square(U_truth[:, :, 2] - U_pred[:, :, 2]))
                   / np.mean(np.square(U_truth[:, :, 2] - np.mean(U_truth[:, :, 2]))))
    RE_p = np.sqrt(np.mean(np.square(p_truth - p_pred))
                   / np.mean(np.square(p_truth - np.mean(p_pred))))
    RE_wss = np.sqrt(np.mean(np.square(wss_pred - wss_truth))
                     / np.mean(np.square(wss_truth - np.mean(wss_truth))))

    ma_U = np.mean(np.abs(mag_U_truth - mag_U_pred), axis=1)
    ma_u = np.mean(np.abs(U_truth[:, :, 0] - U_pred[:, :, 0]), axis=1)
    ma_v = np.mean(np.abs(U_truth[:, :, 1] - U_pred[:, :, 1]), axis=1)
    ma_w = np.mean(np.abs(U_truth[:, :, 2] - U_pred[:, :, 2]), axis=1)
    ma_p = np.mean(np.abs(p_truth - p_pred), axis=1)
    ma_wss = np.mean(np.abs(wss_pred - wss_truth), axis=1)

    MA_U = np.mean(np.abs(mag_U_truth - mag_U_pred))
    MA_u = np.mean(np.abs(U_truth[:, :, 0] - U_pred[:, :, 0]))
    MA_v = np.mean(np.abs(U_truth[:, :, 1] - U_pred[:, :, 1]))
    MA_w = np.mean(np.abs(U_truth[:, :, 2] - U_pred[:, :, 2]))
    MA_p = np.mean(np.abs(p_truth - p_pred))
    MA_wss = np.mean(np.abs(wss_pred - wss_truth))

    t = X[:, 0, 3]

    print()
    print(f'mean relative error:')
    print()
    print(f't   | U     | u     | v     | w     | p     | wss')
    for i in range(t.shape[0]):
        print('%.1f | %.3f | %.3f | %.3f | %.3f | %.3f | %.3f' % (
        float(t[i]), re_U[i], re_u[i], re_v[i], re_w[i], re_p[i], re_wss[i]))
    print('-------------------------------------------')
    print('mean| %.3f | %.3f | %.3f | %.3f | %.3f | %.3f' % (RE_U, RE_u, RE_v, RE_w, RE_p, RE_wss))
    print()

    print()
    print(f'mean absolute error:')
    print()
    print(f't   | U     | u     | v     | w     | p     | wss')
    for i in range(t.shape[0]):
        print('%.1f | %.3f | %.3f | %.3f | %.3f | %.3f | %.3f' % (
            float(t[i]), ma_U[i], ma_u[i], ma_v[i], ma_w[i], ma_p[i], ma_wss[i]))
    print('-------------------------------------------')
    print('mean| %.3f | %.3f | %.3f | %.3f | %.3f | %.3f' % (MA_U, MA_u, MA_v, MA_w, MA_p, MA_wss))
    print()

    t_idx = 5
    x = X[0, :, 0]
    y = X[0, :, 1]
    z = X[0, :, 2]

    pages = [('wss',
              'truth',
              wss_truth[t_idx, :],
              'prediction',
              wss_pred[t_idx, :],
              'difference',
              np.abs(wss_pred[t_idx, :] - wss_truth[t_idx, :]),
              '',
              np.zeros_like(wss_truth[t_idx, :])),
             ('mag(U)',
              'truth',
              mag_U_truth[t_idx, :],
              'prediction',
              mag_U_pred[t_idx, :],
              'difference',
              np.abs(mag_U_truth[t_idx, :] - mag_U_pred[t_idx, :]),
              '',
              []),
             ('u',
              'truth',
              data['U_truth'][t_idx, :, 0],
              'prediction',
              data['U_pred'][t_idx, :, 0],
              'difference',
              np.abs(data['U_truth'][t_idx, :, 0] - data['U_pred'][t_idx, :, 0]),
              '',
              []),
             ('v',
              'truth',
              data['U_truth'][t_idx, :, 1],
              'prediction',
              data['U_pred'][t_idx, :, 1],
              'difference',
              np.abs(data['U_truth'][t_idx, :, 1] - data['U_pred'][t_idx, :, 1]),
              '',
              []),
             ('w',
              'truth',
              data['U_pred'][t_idx, :, 2],
              'prediction',
              data['U_truth'][t_idx, :, 2],
              'difference',
              np.abs(data['U_truth'][t_idx, :, 2] - data['U_pred'][t_idx, :, 2]),
              '',
              []),
             ('p',
              'truth',
              data['p_truth'][t_idx, :],
              'prediction',
              data['p_pred'][t_idx, :],
              'difference',
              np.abs(data['p_truth'][t_idx, :] - data['p_pred'][t_idx, :]),
              '',
              []),
             ('du/dX',
              'dx',
              data['U_X'][t_idx, :, 0, 0],
              'dy',
              data['U_X'][t_idx, :, 0, 1],
              'dz',
              data['U_X'][t_idx, :, 0, 2],
              'dt',
              data['U_X'][t_idx, :, 0, 3]),
             ('dv/dX',
              'dx',
              data['U_X'][t_idx, :, 1, 0],
              'dy',
              data['U_X'][t_idx, :, 1, 1],
              'dz',
              data['U_X'][t_idx, :, 1, 2],
              'dt',
              data['U_X'][t_idx, :, 1, 3]),
             ('dw/dX',
              'dx',
              data['U_X'][t_idx, :, 2, 0],
              'dy',
              data['U_X'][t_idx, :, 2, 1],
              'dz',
              data['U_X'][t_idx, :, 2, 2],
              'dt',
              data['U_X'][t_idx, :, 2, 3]),
             ('dp/dX',
              'dx',
              data['p_X'][t_idx, :, 0],
              'dy',
              data['p_X'][t_idx, :, 1],
              'dz',
              data['p_X'][t_idx, :, 2],
              '',
              []),
             ('du/dX²',
              'dx',
              data['U_XX'][t_idx, :, 0, 0],
              'dy',
              data['U_XX'][t_idx, :, 0, 1],
              'dz',
              data['U_XX'][t_idx, :, 0, 2],
              '',
              []),
             ('dv/dX²',
              'dx',
              data['U_XX'][t_idx, :, 1, 0],
              'dy',
              data['U_XX'][t_idx, :, 1, 1],
              'dz',
              data['U_XX'][t_idx, :, 1, 2],
              '',
              []),
             ('dw/dX²',
              'dx',
              data['U_XX'][t_idx, :, 2, 0],
              'dy',
              data['U_XX'][t_idx, :, 2, 1],
              'dz',
              data['U_XX'][t_idx, :, 2, 2],
              '',
              [])]

    fig = plt.figure()

    ax_1 = fig.add_subplot(221, projection='3d')
    ax_2 = fig.add_subplot(222, projection='3d')
    ax_3 = fig.add_subplot(223, projection='3d')
    ax_4 = fig.add_subplot(224, projection='3d')


    def change_data(c_data):
        x_tmp = x if c_data[2].shape[0] == x.shape[0] else x[-c_data[2].shape[0]:]
        y_tmp = y if c_data[2].shape[0] == y.shape[0] else y[-c_data[2].shape[0]:]
        z_tmp = z if c_data[2].shape[0] == z.shape[0] else z[-c_data[2].shape[0]:]
        fig.suptitle(c_data[0])
        ax_1.set_title(c_data[1])
        ax_1.scatter(x_tmp, z_tmp, y_tmp, c=c_data[2])
        ax_1.view_init(elev=def_elev, azim=def_azim, roll=def_roll)
        ax_1.set_xlabel('x')
        ax_1.set_ylabel('z')
        ax_1.set_zlabel('y')
        ax_2.set_title(c_data[3])
        ax_2.scatter(x_tmp, z_tmp, y_tmp, c=c_data[4])
        ax_2.view_init(elev=def_elev, azim=def_azim, roll=def_roll)
        ax_2.set_xlabel('x')
        ax_2.set_ylabel('z')
        ax_2.set_zlabel('y')
        ax_3.set_title(c_data[5])
        ax_3.scatter(x_tmp, z_tmp, y_tmp, c=c_data[6])
        ax_3.view_init(elev=def_elev, azim=def_azim, roll=def_roll)
        ax_3.set_xlabel('x')
        ax_3.set_ylabel('z')
        ax_3.set_zlabel('y')
        ax_4.set_title(c_data[7])
        ax_4.view_init(elev=def_elev, azim=def_azim, roll=def_roll)
        if isinstance(c_data[8], list):
            ax_4.scatter(x_tmp, z_tmp, y_tmp, c=c_data[8])
            ax_4.set_xlabel('x')
            ax_4.set_ylabel('z')
            ax_4.set_zlabel('y')
        else:
            ax_4.scatter([], [], [], c=[])
            ax_4.set_xlabel('')
            ax_4.set_ylabel('')
            ax_4.set_zlabel('')
        plt.draw()


    class Index:
        def __init__(self):
            self.idx = 0
            c_d = pages[self.idx]
            change_data(c_data=c_d)

        def next(self, _):
            self.idx += 1
            if self.idx >= len(pages):
                self.idx = 0
            c_d = pages[self.idx]
            change_data(c_data=c_d)

        def prev(self, _):
            self.idx -= 1
            if self.idx < 0:
                self.idx = len(pages) - 1
            c_d = pages[self.idx]
            change_data(c_data=c_d)


    callback = Index()
    axprev = fig.add_axes((0.7, 0.05, 0.1, 0.075))
    axnext = fig.add_axes((0.81, 0.05, 0.1, 0.075))
    bnext = Button(axnext, 'Next')
    bnext.on_clicked(callback.next)
    bprev = Button(axprev, 'Previous')
    bprev.on_clicked(callback.prev)

    plt.show()
