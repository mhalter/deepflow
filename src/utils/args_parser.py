from argparse import Namespace, ArgumentParser

from .parameters import default_parameters

parser = ArgumentParser(prog='PINN',
                        description='train and evaluate a PINN')

parser.add_argument('--id',
                    type=str,
                    help='job identifier')
parser.add_argument('--path',
                    type=str,
                    help='path to repository')
parser.add_argument('--case-name',
                    type=str,
                    help='name of the dataset')
parser.add_argument('--train-set-name',
                    type=str,
                    help='name of the subset')
parser.add_argument('--test-set-name',
                    type=str,
                    help='name of the subset')
parser.add_argument('--batch-size',
                    type=int,
                    help='size of residual batches')
parser.add_argument('--learning-rate',
                    type=float,
                    help='learning rate used by the optimizer')
parser.add_argument('--hidden-channels',
                    type=int,
                    help='dimensions of hidden layers')
parser.add_argument('--depth',
                    type=int,
                    help='number of layers')
parser.add_argument('--activation',
                    type=str,
                    help='activation function used by the NN')
parser.add_argument('--w-data',
                    type=float,
                    help='weight for the prediction loss')
parser.add_argument('--w-residual',
                    type=float,
                    help='wieght for the residual loss')
parser.add_argument('--max-epochs',
                    type=int,
                    help='Maximum number of training epochs')
parser.add_argument('--training-fraction',
                    type=float,
                    help='fraction of the data used during training')
parser.add_argument('--boundary-fraction',
                    type=float,
                    help='fraction of the data (at the wall) used during training')
parser.add_argument('--seed',
                    type=int,
                    help='seed for random generators')
parser.add_argument('--batch-norm',
                    type=bool,
                    help='normalize batches')


def get_parameters() -> Namespace:
    return parser.parse_args(namespace=default_parameters)
